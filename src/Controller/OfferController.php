<?php

namespace App\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\Annotations\FileParam;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security as OASecurity;
use App\Entity\Article;
use App\Utils\CacheHelper;

/**
 * @Route("/api/offer")
 */
class OfferController extends AbstractBaseController
{
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @OA\Get(description="Get list of articles", tags={"Article"})
     * @OA\Response(
     *     response=200,
     *     description="OK",
     *     @OA\JsonContent(type="array", @OA\Items(ref=@Model(type=Article::class, groups={"Article:short"})))
     * )
     * @QueryParam(name="full", requirements="(true|false)", default="false")
     *
     * @Rest\Get("/", name="app_article_list")
     * @Rest\View(serializerGroups={"Article:short"})
     */
    public function list(CacheHelper $cacheHelper, ParamFetcher $paramFetcher): array
    {
        $full = $paramFetcher->get('full') === 'true';

        if ($full) {
            return $cacheHelper->getAll(Article::class, 'full');
        }

        return $cacheHelper->get(Article::class, null, 'getActivated');
    }
}